import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MemberPage } from './member.page';
import { Routes, RouterModule } from '@angular/router';

const MEMBER_ROUTES:Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: MemberPage
  }
]

@NgModule({
  declarations: [MemberPage],
  imports: [
    CommonModule,
    RouterModule.forChild(MEMBER_ROUTES)
  ]
})
export class MemberModule { }
