import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubLayoutComponent } from './sub-layout.component';
import { SubLayoutRoutingModule } from './sub-layout-routing.module';



@NgModule({
  declarations: [SubLayoutComponent],
  imports: [
    CommonModule,
    SubLayoutRoutingModule
  ]
})
export class SubLayoutModule { }
